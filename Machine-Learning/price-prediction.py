import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

# Load the dataset (replace with your dataset path)
#data = pd.read_csv("Real estate valuation data set.xlsx")

data = pd.read_excel("Real estate valuation data set.xlsx")

# Select features and the target variable
X = data[['X3 distance to the nearest MRT station', 'X4 number of convenience stores']]
y = data['Y house price of unit area']

# Split data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Create the model
model = LinearRegression()

# Train the model
model.fit(X_train, y_train)

# Make predictions
y_pred = model.predict(X_test)

# Evaluate the model's accuracy
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)


print(data.head())  # See the first few rows

print(data.describe())  # Get summary statistics
