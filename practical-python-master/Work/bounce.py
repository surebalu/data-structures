# bounce.py
#
# Exercise 1.5

dropped_height = 100
drop_count = 0

while drop_count < 10:
    dropped_height = dropped_height * (3/5)
    drop_count += 1
    print(drop_count, round(dropped_height, 4))
