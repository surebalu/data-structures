def is_prime(n):
  """
  This function checks if a number is prime.

  Args:
    n: An integer to check.

  Returns:
    True if n is prime, False otherwise.
  """

  if n <= 1:
    return False
  elif n <= 3:
    return True
  elif n % 2 == 0 or n % 3 == 0:
    return False

  i = 5
  while i * i <= n:
    if n % i == 0 or n % (i + 2) == 0:
      return False
    i += 6

  return True

# Check if a few numbers are prime
print(is_prime(7))  # Output: True
print(is_prime(10))  # Output: False
print(is_prime(29))  # Output: True
