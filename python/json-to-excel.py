import pandas as pd
import json

def process_json_to_excel(json_file_path):
    try:
        # Read JSON data from file
        with open(json_file_path, "r") as file:
            try:
                data = json.load(file)
                if isinstance(data, dict):
                    # Convert single dictionary to list of dictionaries
                    data = [data]
            except json.JSONDecodeError:
                print("Invalid JSON format.")
                return

        print("Loaded JSON data:")
        print(data)

        # Flatten nested JSON and create DataFrame
        records = []
        for item in data:
            print("Processing item:")
            print(item)
            record = {}
            for key, value in item.items():
                if isinstance(value, dict):
                    for sub_key, sub_value in value.items():
                        record[f"{key}_{sub_key}"] = sub_value
                elif isinstance(value, list):
                    for i, sub_item in enumerate(value):
                        for sub_key, sub_value in sub_item.items():
                            record[f"{key}_{i+1}_{sub_key}"] = sub_value
                else:
                    record[key] = value
            records.append(record)

        df = pd.DataFrame(records)

        # Output DataFrame to Excel
        excel_file_path = "flattened_data.xlsx"
        df.to_excel(excel_file_path, index=False)

        print(f"Excel file saved to: {excel_file_path}")
    except FileNotFoundError:
        print("File not found!")
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    json_file_path = input("Enter the JSON file path: ")
    process_json_to_excel(json_file_path)
