student_heights = input("Enter the student heights :: ").split()
for n in range(0, len(student_heights)):
    student_heights[n] = int(student_heights[n])
total_height = 0
num_of_students = 0
for height in student_heights:
    total_height += height
    num_of_students = num_of_students + 1

avg_height = total_height / num_of_students

print(f"total height = {total_height}")
print(f"number of students = {num_of_students}")
print(f"average height = {round(avg_height)}")