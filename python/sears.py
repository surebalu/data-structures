# sears.py
bill_thickness = 0.11 * 0.001  # in meters
sears_height = 442  # in meters
day = 1
bill_count = 1

while bill_count * bill_thickness <= sears_height:
    day += 1
    bill_count = bill_count * 2
print(f"It took {bill_count} bills and {day} days to reach the top of sears")
