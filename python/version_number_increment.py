import os
import datetime
def increment_version_with_year(file_path):
    # Read the current version from the file
    with open(file_path, 'r') as file:
        content = file.read()
    try:
        year, version_number = map(int, content.split('.'))
        current_year = datetime.datetime.now().year
        if year != current_year:
            year = current_year
            version_number = 1
        else:
            version_number += 1
    except ValueError:
        print("The file doesn't contain a valid version format.")
    updated_content = f"{year}.{version_number}"
    with open(file_path, 'w') as file:
        file.write(updated_content)
current_dir = os.getcwd()
parent_dir = os.path.dirname(current_dir)
os.chdir(os.path.join(parent_dir, "Riv3DXEnv-R2023x"))
version_file_directory = os.getcwd() + "/Riv3DXEnv-R2023x.ver"
file_path = version_file_directory
increment_version_with_year(file_path)
