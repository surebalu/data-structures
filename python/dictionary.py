
dict_test = {
    "name": "John",
    "age": 30,
    "is_verified": True,
}

print(dict_test["name"])
print(dict_test.get("name"))
print(dict_test.get("birthdate", "Jan 1 1980"))
dict_test["name"] = "Jane"
print(dict_test["name"])
dict_test["birthdate"] = "Jan 1 1980"
print(dict_test["birthdate"])
print(dict_test)
dict_test["Country"] = "USA"
print(dict_test.get("is_verified"))
