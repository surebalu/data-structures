print("Welcome to Love Calculator..")
name1 = input("Enter your name :: ")
name2 = input("Enter your partner name :: ")
combined_name = name1 + name2
lower_case_names = combined_name.lower()
t = lower_case_names.count("t")
r = lower_case_names.count("r")
u = lower_case_names.count("u")
e = lower_case_names.count("e")
first_number = t+r+u+e
l = lower_case_names.count("l")
o = lower_case_names.count("o")
v = lower_case_names.count("v")
e = lower_case_names.count("e")
second_number = l+o+v+e
total_score = str(first_number) + str(second_number)
converted_score = int(total_score)
if converted_score < 10 or converted_score > 90:
    print(
        f"Your score is {total_score}, you go together like coke and mentos.")
elif 39 < converted_score < 51:
    print(f"Your score is {total_score}, you are alright together.")
else:
    print(f"Your score is {total_score}.")
