import math
print("Welcome to Python Pizza")
size = input("What size of pizza you would like to order? :: ")
need_pepperoni = input("Do you want to add pepperoni to your pizza? :: ")
need_cheese = input("Do you want to add extra cheese to your pizza? :: ")
if size == 'S':
    bill = 15
    if need_pepperoni == 'Y':
        bill = bill+2
elif size == 'M':
    bill = 20
    if need_pepperoni == 'Y':
        bill = bill+3
else:
    bill = 25
    if need_pepperoni == 'Y':
        bill = bill+3
if need_cheese == 'Y':
    bill = bill+1
print(f"Your final bill is: ${bill}.")