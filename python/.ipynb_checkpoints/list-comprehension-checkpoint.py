items = [
    ("Product1", 60),
    ("Product2", 20),
    ("Product3", 30),
]
filtered = list(filter(lambda item: item[1] >= 12, items))
print(filtered)
filtered_list = [item for item in items if item[1] >= 11]

print(filtered_list)

prices = list(map(lambda item: item[1], items))
prices.sort(reverse=True)
print(prices)