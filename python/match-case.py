
x = int(input("Enter a number :: "))

match x:
    case 10:
        print(f"Your number is {x}")
    case 4 if x < 4:
        print(f"Your number is is {x}")
    case _ if x > 30:
        print(f"Your number is is  is {x}")
    case 40:
        print(f"Your number is {x}")
    case _:
        print(f"Your number is {x}")
