
def fizzbuzz(n: int):
    if n % 3 == 0 and n % 5 != 0:
        return "Fizz"
    elif n % 5 == 0 and n % 3 != 0:
        return "Buzz"
    elif n % 3 == 0 and n % 5 == 0:
        return "FizzBuzz"
    else:
        return f"{n}"


for n in range(1, 101):
    print(fizzbuzz(n))

